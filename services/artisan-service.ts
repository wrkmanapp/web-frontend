import axios, { AxiosResponse } from "axios"
import { ApiData } from "~/types/api"
import { IServiceArtisans } from "~/types/content"

const FETCH_ARTISAN = "/app/artisan"

export class artisanService {
  /**
   *
   * @param id
   */
  static fetchArtisan = (id: string): Promise<ApiData<IServiceArtisans>> => {
    const url = `${process.env.BASE_URL}${FETCH_ARTISAN}/${id}`
    return axios
      .get(url)
      .then(
        (response: AxiosResponse<ApiData<IServiceArtisans>>) => response.data
      )
  }
}
