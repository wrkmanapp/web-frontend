import axios, { AxiosResponse } from "axios"
import { ApiData } from "~/types/api"
import { loginData } from "~/types/services"

const LOGIN = "/user/login"

export class userService {
  /**
   * Login
   * @param formData Object
   * @returns Promise
   */
  static login = (formData: Object): Promise<ApiData<loginData>> => {
    const url = `${LOGIN}`
    return axios
      .post(url, formData)
      .then((response: AxiosResponse<ApiData<loginData>>) => response.data)
  }
}
