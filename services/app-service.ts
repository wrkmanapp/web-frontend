import axios, { AxiosResponse } from "axios"
import { ApiData } from "~/types/api"
import { IServiceArtisans, IServicesContent } from "~/types/content"
import { ArtisansAroundPayload } from "~/types/services"

const FETCH_SERVICES = "/app/services"
const FETCH_ARTISANS_AROUND = "/api/fetch-artisans-around"

export class appService {
  /**
   * FetchAllServices
   * @returns Promise
   */
  static fetchAllServices = (): Promise<ApiData<IServicesContent[]>> => {
    const url = `${process.env.BASE_URL}${FETCH_SERVICES}`
    return axios
      .get(url)
      .then(
        (response: AxiosResponse<ApiData<IServicesContent[]>>) => response.data
      )
  }

  /**
   * Fetches artisans around for a service
   * @param formData
   */

  static fetchArtisansAround = (
    formData: ArtisansAroundPayload
  ): Promise<ApiData<IServiceArtisans[]>> => {
    const url = `${FETCH_ARTISANS_AROUND}`

    return axios
      .post(url, formData)
      .then(
        (response: AxiosResponse<ApiData<IServiceArtisans[]>>) => response.data
      )
  }
}
