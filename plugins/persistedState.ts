import { Plugin } from "@nuxt/types"
import createPersistedState from "vuex-persistedstate"

const persistedStatePlugin: Plugin = ({ app, store, res }) => {
  createPersistedState({
    paths: ["auth.token", "auth.user"],
    storage: {
      getItem: (key) => {
        return JSON.stringify(app.$cookies.get(key))
      },
      setItem: (key, value) => {
        if (!res || (res && !res.headersSent)) {
          app.$cookies.set(key, value)
        }
      },
      removeItem: (key) => app.$cookies.remove(key),
    },
  })(store)
}

export default persistedStatePlugin
