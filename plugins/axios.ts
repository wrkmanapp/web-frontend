import { Plugin } from "@nuxt/types"
declare module "axios" {
  interface AxiosResponse {
    handleError: Boolean
  }

  interface AxiosRequestConfig {
    handleError: Boolean
  }
}

const axiosPlugin: Plugin = ({ $axios, app, store, redirect }) => {
  $axios.onRequest((_config: any) => {
    const token = store.getters["auth/getToken"]

    if (token) {
      $axios.setHeader("Authorization", "Bearer " + token)
    }
  })

  $axios.onError((error) => {
    // if error has to be handled in the main code, handleError would be passed in the config as false
    if (
      Object.prototype.hasOwnProperty.call(error.config, "handleError") &&
      !error?.config?.handleError
    ) {
      return Promise.reject(error)
    }

    if (
      error.response?.config.url !== "/login" &&
      error.response?.status === 401
    ) {
      app.$clearStore({ store })
      app.$cookies.remove("vuex")
      localStorage.removeItem("vuex")
      redirect("/login")
      return
    }

    return app.$errorHandler(error)
  })
}

export default axiosPlugin
