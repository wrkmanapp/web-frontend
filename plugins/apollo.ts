import { Context } from "@nuxt/types"

export default (context: Context) => {
  const { $config } = context
  return {
    httpEndpoint: `${$config.strHost}/graphql`,
    ssr: true,
    ssrMode: true,
  }
}
