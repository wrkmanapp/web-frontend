import Vue from 'vue'
import Element from 'element-ui'
declare module 'element-ui'

export default () => {
  Vue.use(Element)
}
