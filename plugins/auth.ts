import { Plugin, NuxtConfig } from "@nuxt/types"

declare module "@nuxt/types" {
  interface Context {
    $logout(message: string): void
  }
}

const clearStore = async ({ store }: NuxtConfig) => {
  // clearing Auth module
  await store.dispatch("auth/logout")
}

const logout = async ({ app, redirect, store }: NuxtConfig) => {
  await clearStore({ store })
  localStorage.removeItem("vuex")
  app.$cookies.remove("vuex")
  redirect("/login")
}

const authPlugin: Plugin = (context, inject) => {
  inject("logout", () => logout(context))
  inject("clearStore", clearStore)
}

export default authPlugin
