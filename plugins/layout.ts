import { Plugin } from "@nuxt/types"
import { getCurrentLocation } from "~/utils/globals"

declare module "@nuxt/types" {
  interface Context {
    $getCurrentLocation(): Promise<GeolocationPosition>
  }
}

declare module "vue/types/vue" {
  interface Vue {
    $getCurrentLocation(): Promise<GeolocationPosition>
  }
}

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const layoutPlugin: Plugin = (context, inject) => {
  inject("getCurrentLocation", getCurrentLocation)
}

export default layoutPlugin
