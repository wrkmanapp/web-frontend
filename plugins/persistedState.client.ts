import { Plugin } from "@nuxt/types"
import createPersistedState from "vuex-persistedstate"

const persistedStatePlugin: Plugin = ({ store }) => {
  createPersistedState({
    paths: ["auth.user"],
  })(store)
}

export default persistedStatePlugin
