// eslint-disable-next-line
require('intersection-observer') // polyfill
// eslint-disable-next-line
import Vue from 'vue'
// eslint-disable-next-line
import VueObserveVisibility from 'vue-observe-visibility'

Vue.use(VueObserveVisibility)
