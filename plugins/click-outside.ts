import Vue from 'vue'

Vue.directive('click-outside', {
  bind(el, binding, vNode) {
    const vm = vNode.context
    const callback = binding.value

    const onClickOut = (event: any) => {
      if (!(el === event.target || el.contains(event.target))) {
        return callback.call(vm, event)
      }
    }
    // @ts-ignore
    el.onClickOut = onClickOut
    document.body.addEventListener('click', onClickOut)
  },
  unbind(el) {
    // @ts-ignore
    document.body.removeEventListener('click', el.onClickOut)
  },
})

Vue.directive('focus-away', {
  bind(el, binding, vNode) {
    const vm = vNode.context
    const callback = binding.value

    const onClickOut = (event: any) => {
      if (!(el === event.target || el.contains(event.target))) {
        return callback.call(vm, event)
      }
    }
    // @ts-ignore
    el.onClickOut = onClickOut
    document.body.addEventListener('focus', onClickOut)
  },
  unbind(el) {
    // @ts-ignore
    document.body.removeEventListener('focus', el.onClickOut)
  },
})
