import { NuxtConfig, Plugin } from "@nuxt/types"
import Vue from "vue"
import { Notification } from "element-ui"
import { AxiosError } from "axios"

Vue.prototype.$notify = Notification

declare module "@nuxt/types" {
  interface Context {
    $errorHandler(message: string): void
  }
}
const showDefaultNotification = (message: string): void => {
  Vue.prototype.$notify({
    type: "error",
    title: "Error",
    message,
  })
}

const showValidationNotification = (errors: Object, title: string): void => {
  const errorCount = Object.values(errors).length

  if (errorCount < 1) {
    return showDefaultNotification(title)
  }

  const message = Object.values(errors)[0][0]

  Vue.prototype.$notify({
    type: "error",
    title,
    message,
  })
}

const errorCodeHandler = (code: string, context: NuxtConfig): void => {
  switch (code) {
    case "invalid_device_id":
      context.app.router.push("/add-device")
      break

    default:
  }
}

const errorHandler = (error: AxiosError, context: NuxtConfig): void => {
  const data = error.response?.data
  const status = error.response?.status
  const message: string = data ? data.message : "An Error Occured"

  if (data?.error_code) {
    errorCodeHandler(data.error_code, context)
  }

  switch (status) {
    // handle validation errors
    case 422:
      showValidationNotification(data.errors, message)
      break

    // any other generic notification
    default:
      showDefaultNotification(message)
  }
}

const errorHandlerPlugin: Plugin = (context, inject) => {
  inject("errorHandler", (error: AxiosError): void => {
    errorHandler(error, context)
  })
}

export default errorHandlerPlugin
