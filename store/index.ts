// eslint-disable-next-line import/named
import { ActionTree } from "vuex"
import { Store } from "vuex"
import { auth } from "./auth"
import { services } from "./services"
import { LayoutState } from "~/types/store"
import { layout } from "~/store/layout"
import { home } from "~/store/home"
import { about } from "~/store/about"
import { artisan } from "~/store/artisan"
type RootState = {
  layout: LayoutState
}

export default () =>
  new Store({
    modules: {
      auth,
      services,
      home,
      about,
      artisan,
      layout,
    },
    actions,
  })

export const actions: ActionTree<RootState, RootState> = {
  async nuxtServerInit(
    { dispatch },
    {
      app: {
        apolloProvider: { defaultClient: $apollo },
      },
    }
  ) {
    await dispatch("layout/getData", {
      apolloQuery: $apollo.query,
    })
    await dispatch("layout/getContact", {
      apolloQuery: $apollo.query,
    })
  },
}
