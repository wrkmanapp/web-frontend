// eslint-disable-next-line import/named
import { ActionTree, GetterTree, Module, MutationTree } from "vuex"
import Vue from "vue"
import { IArtisanState, IArtisanState as State } from "~/types/store"
import { IServiceArtisans } from "~/types/content"
import { artisanService } from "~/services/artisan-service"

export const state = (): State => ({
  data: undefined,
})

export type RootState = ReturnType<typeof state>

export const mutations: MutationTree<RootState> = {
  setArtisan(s: State, data: IArtisanState[]): void {
    Vue.set(s, "data", data)
  },
}

export const getters: GetterTree<State, RootState> = {
  data(s: State): IServiceArtisans | undefined {
    return s.data
  },
}

export const actions: ActionTree<State, RootState> = {
  async getData({ commit }, { id }): Promise<void> {
    const res = await artisanService.fetchArtisan(id)

    commit("setArtisan", res.data)
  },
}

export const artisan: Module<State, any> = {
  namespaced: true,
  state,
  getters,
  mutations,
  actions,
}
