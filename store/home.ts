// eslint-disable-next-line import/named
import { ActionTree, GetterTree, Module, MutationTree } from "vuex"
import Vue from "vue"
import query from "~/apollo/home.gql"
import { HomeState as State } from "~/types/store"
import { IHomeContent } from "~/types/content"

export const state = (): State => ({
  data: undefined,
})

export type RootState = ReturnType<typeof state>

export const mutations: MutationTree<RootState> = {
  setHome(s: State, data: IHomeContent): void {
    Vue.set(s, "data", data)
  },
}

export const getters: GetterTree<State, RootState> = {
  data(s: State): IHomeContent | undefined {
    return s.data
  },
  seoTitle(s: State): string {
    return s.data?.seoInfo?.pageTitle ?? "home"
  },
  seoPageDescription(s: State): string {
    return s.data?.seoInfo?.pageDescription ?? ""
  },
}

export const actions: ActionTree<State, RootState> = {
  async getData({ commit }, { id, locale, apolloQuery }): Promise<void> {
    const variables = { id, locale }

    const res = await apolloQuery({
      query,
      variables,
    })

    commit("setHome", res.data.home)
  },
}

export const home: Module<State, any> = {
  namespaced: true,
  state,
  getters,
  mutations,
  actions,
}
