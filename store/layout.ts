// eslint-disable-next-line import/named
import { ActionTree, GetterTree, Module, MutationTree } from "vuex"
import Vue from "vue"
import { LayoutState as State } from "~/types/store"
import { IContactContent, ILayoutContent } from "~/types/content"

import query from "~/apollo/layout.gql"
import queryFeature from "~/apollo/contact.gql"

export const state = (): State => ({
  data: undefined,
  contact: undefined,
})

export type RootState = ReturnType<typeof state>

export const mutations: MutationTree<RootState> = {
  setLayout(s: State, data: ILayoutContent): void {
    Vue.set(s, "data", data)
  },
  setContact(s: State, data: IContactContent): void {
    Vue.set(s, "contact", data)
  },
}

export const getters: GetterTree<State, RootState> = {
  data(s: State): ILayoutContent | undefined {
    return s.data
  },
  contact(s: State): IContactContent | undefined {
    return s.contact
  },
}

export const actions: ActionTree<State, RootState> = {
  async getData({ commit }, { locale, apolloQuery }): Promise<void> {
    const variables = { locale }

    const res = await apolloQuery({
      query,
      variables,
    })

    commit("setLayout", res.data.layout)
  },

  async getContact({ commit }, { locale, apolloQuery }): Promise<void> {
    const variables = { locale }

    const res = await apolloQuery({
      query: queryFeature,
      variables,
    })

    commit("setContact", res.data.contact)
  },
}

export const layout: Module<State, any> = {
  namespaced: true,
  state,
  getters,
  mutations,
  actions,
}
