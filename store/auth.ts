// eslint-disable-next-line import/named
import { ActionTree, GetterTree, MutationTree, Module } from "vuex"
import Vue from "vue"
import { AuthState as State } from "~/types/store"
import { User } from "~/types/content"
import { userService } from "~/services/user-service"
export const state = (): State => ({
  loading: false,
  user: {},
  token: "",
})

export type RootState = ReturnType<typeof state>

export const mutations: MutationTree<RootState> = {
  setToken(s: State, token: string): void {
    Vue.set(s, "token", token)
  },
  setUser(s: State, data: User): void {
    Vue.set(s, "user", data)
  },

  setUserLocation(s: State, data: string): void {
    Vue.set(s.user, "location", data)
  },
}

export const getters: GetterTree<State, RootState> = {
  getUser(s: State): User | undefined {
    return s.user
  },
  getToken(s: State): string | undefined {
    return s.token
  },
}

export const actions: ActionTree<State, RootState> = {
  async login({ commit }, payload): Promise<void> {
    const res = await userService.login({ ...payload })
    const { token, user } = res.data
    commit("setToken", token)
    commit("setUser", user)
  },

  updateLocation({ commit }, payload): void {
    commit("setUserLocation", payload)
  },

  logout({ commit }) {
    commit("setToken", "")
    commit("setUser", {})
  },
}

export const auth: Module<State, any> = {
  namespaced: true,
  state,
  getters,
  mutations,
  actions,
}
