// eslint-disable-next-line import/named
import { ActionTree, GetterTree, Module, MutationTree } from "vuex"
import Vue from "vue"
import { IServicesState as State } from "~/types/store"
import {
  IServiceArtisans,
  IServicesContent,
  TServiceContent,
} from "~/types/content"
import { appService } from "~/services/app-service"

import query from "~/apollo/service.gql"
export const state = (): State => ({
  data: undefined,
  service: undefined,
  serviceArtisans: undefined,
})

export type RootState = ReturnType<typeof state>

export const mutations: MutationTree<RootState> = {
  setServices(s: State, data: IServicesContent[]): void {
    Vue.set(s, "data", data)
  },
  setServiceArtisans(s: State, data: IServiceArtisans[]): void {
    Vue.set(s, "serviceArtisans", data)
  },
  setService(s: State, data: TServiceContent): void {
    Vue.set(s, "service", data)
  },
}

export const getters: GetterTree<State, RootState> = {
  data(s: State): IServicesContent[] | undefined {
    return s.data
  },
  serviceArtisans(s: State): IServiceArtisans[] | undefined {
    return s.serviceArtisans
  },
  serviceContent(s: State): TServiceContent | undefined {
    return s.service
  },
  seoTitle(s: State): string {
    return s.service?.seoInfo?.pageTitle ?? "Services"
  },
  seoPageDescription(s: State): string {
    return s.service?.seoInfo?.pageDescription ?? ""
  },
}

export const actions: ActionTree<State, RootState> = {
  async getData({ commit }): Promise<void> {
    const res = await appService.fetchAllServices()

    commit("setServices", res.data)
  },

  async getService({ commit }, { locale, apolloQuery }): Promise<void> {
    const variables = { locale }

    const res = await apolloQuery({
      query,
      variables,
    })
    commit("setService", res.data.service)
  },

  async getArtisans({ commit }, payload): Promise<void> {
    const res = await appService.fetchArtisansAround(payload)
    commit("setServiceArtisans", res.data)
  },
}

export const services: Module<State, any> = {
  namespaced: true,
  state,
  getters,
  mutations,
  actions,
}
