// eslint-disable-next-line import/named
import { ActionTree, GetterTree, Module, MutationTree } from "vuex"
import Vue from "vue"
import query from "~/apollo/about.gql"
import { AboutState as State } from "~/types/store"
import { IAboutContent } from "~/types/content"

export const state = (): State => ({
  data: undefined,
})

export type RootState = ReturnType<typeof state>

export const mutations: MutationTree<RootState> = {
  setAbout(s: State, data: IAboutContent): void {
    Vue.set(s, "data", data)
  },
}

export const getters: GetterTree<State, RootState> = {
  data(s: State): IAboutContent | undefined {
    return s.data
  },
  seoTitle(s: State): string {
    return s.data?.seoInfo?.pageTitle ?? "About"
  },
  seoPageDescription(s: State): string {
    return s.data?.seoInfo?.pageDescription ?? ""
  },
}

export const actions: ActionTree<State, RootState> = {
  async getData({ commit }, { id, locale, apolloQuery }): Promise<void> {
    const variables = { id, locale }

    const res = await apolloQuery({
      query,
      variables,
    })

    commit("setAbout", res.data.about)
  },
}

export const about: Module<State, any> = {
  namespaced: true,
  state,
  getters,
  mutations,
  actions,
}
