<template>
  <div
    :class="{
      mobile: $device.isMobile,
      tablet: $device.isTablet,
      desktop: $device.isDesktop,
      ios: $device.isIos,
      android: $device.isAndroid,
      mac: $device.isMacOS,
      windows: $device.isWindows,
    }"
  >
    <div v-observe-visibility="anchorScrolledOptions" class="top-anchor" />

    <app-nav
      :class="{ 'footer-top': footerAtTop, 'footer-visible': footerVisible }"
      :has-dark-hero="hasDarkHero(getRouteBaseName())"
      :has-hero="hasHero(getRouteBaseName()) && !hasError"
      :is-scrolled="isScrolled || loading"
      :routes="routes"
    />

    <main
      id="app-main"
      :class="{
        'footer-visible': footerVisible,
      }"
    >
      <Nuxt @error="(val) => (hasError = val)" />
      <transition
        enter-active-class="transition ease-out duration-100"
        enter-from-class="transform opacity-0 scale-95"
        enter-to-class="transform opacity-100 scale-100"
        leave-active-class="transition ease-in duration-100"
        leave-from-class="transform opacity-100 scale-100"
        leave-to-class="transform opacity-0 scale-95"
      >
        <div v-if="loading" class="page-loader">
          <div class="w-20 mx-auto bg-primary-400 rounded-full"></div>
        </div>
      </transition>
    </main>

    <footer
      v-if="!hasError"
      id="app-footer"
      v-observe-visibility="footerAtTopOptions"
    >
      <contact />
      <app-footer
        v-observe-visibility="footerVisibilityOptions"
        :get-help="data.footerGetHelpTitle"
        :company="data.footerCompanyTitle"
        :get-help-links="data.getHelpLinks"
        :company-links="data.companyLinks"
        :social-links="data.mediaLinks"
        :logo="data.footerLogo"
        :copyright="data.copyrightText"
      />
    </footer>
  </div>
</template>

<script lang="ts">
import {
  computed,
  defineComponent,
  onMounted,
  reactive,
  ref,
} from "@nuxtjs/composition-api"
import { mapGetters } from "vuex"
import AppNav from "~/components/nav/AppNav.vue"
import AppFooter from "~/components/footer/Index.vue"
import { PageRoutes } from "~/types/page"
import Contact from "~/components/footer/Contact.vue"

export default defineComponent({
  name: "DefaultLayout",

  components: {
    Contact,
    AppNav,
    AppFooter,
  },
  middleware: ["auth"],
  setup(_, { root }) {
    /**
     * Holds the current component instance
     */

    /**
     * Computed property that returns the app's routes
     */
    const routes = computed<PageRoutes>(() => {
      return [
        {
          to: { name: "services" },
          name: "Services",
        },
        {
          to: { name: "about" },
          name: "About Us",
        },
      ]
    })
    /**
     * Holds the loading state
     */
    const loading = ref(true)

    /**
     * Holds state, whether user has scrolled or not
     */
    const isScrolled = ref(false)

    /**
     * Holds state, whether there's an error
     */
    const hasError = ref(false)

    /**
     * Holds state, whether footer has scrolled into view and has reached the top of the page
     */
    const footerAtTop = ref(false)

    /**
     * Holds state, whether footer is in viewport
     */
    const footerVisible = ref(false)

    /**
     * List of page names that have hero at the top
     */
    const pagesWithHero = ref<string[]>(["index", "about"])

    /**
     * List of page names that have hero at the top with dark background
     */
    const pagesWithDarkHero = ref<string[]>(["services", "services-id"])

    /**
     * List of page names that the user has visited in the current session
     */
    const visitedPages = reactive<Array<string | null | undefined>>([])

    /**
     * Checks if the current page has hero
     * @param {string} name - the name of the route/page
     * @returns {boolean}
     */
    const hasHero = (name: string) => pagesWithHero.value.includes(name)

    /**
     * Checks if the current page has a dark hero
     * @param {string} name - the name of the route/page
     * @returns {boolean}
     */
    const hasDarkHero = (name: string) => pagesWithDarkHero.value.includes(name)

    /**
     * Sets the scrolled state of the app
     * @param {boolean} isVisible
     */
    const anchorScrolled = (isVisible: boolean) => {
      isScrolled.value = !isVisible
    }

    /**
     * Sets the footer visibility state of the app
     * @param {boolean} isVisible
     */
    const footerAtTopChanged = (isVisible: boolean): void => {
      footerAtTop.value = isVisible
    }

    /**
     * Sets the footer visibility state of the app
     * @param {boolean} isVisible
     */
    const footerVisibilityChanged = (isVisible: boolean): void => {
      footerVisible.value = isVisible
    }

    /**
     * Scroll observer options
     * @type {{callback: function, threshold: number[]}}
     */
    const anchorScrolledOptions = { callback: anchorScrolled, threshold: [1] }

    /**
     * Scroll observer options
     * @type {{intersection: {rootMargin: string, threshold: number[]}, callback: function}}
     */
    const footerAtTopOptions = {
      callback: footerAtTopChanged,
      intersection: { threshold: 0.5, rootMargin: "64px 0px 0px 0px" },
    }

    /**
     * Scroll observer options
     * @type {{intersection: {rootMargin: string, threshold: number[]}, callback: function}}
     */
    const footerVisibilityOptions = {
      callback: footerVisibilityChanged,
      intersection: { threshold: 0.0, rootMargin: "0px" },
    }

    const setLoading = () => {
      root.$nextTick(() => {
        // reset loading state
        setTimeout(() => {
          loading.value = false
        }, 1000)
      })
    }

    onMounted(() => {
      // save current page path so we don't show loader next time
      visitedPages.push(root.$route.path)
      setLoading()
    })

    // reset loading state after each route navigation
    root.$router.afterEach(() => {
      if (loading.value) {
        setLoading()
      }
    })

    // set loading state on each route navigation
    root.$router.beforeEach((to, _from, next) => {
      // show loader if we've not visited this page in the current session
      if (!visitedPages.includes(to.path)) {
        visitedPages.push(to.path)
        loading.value = true
      }
      next()
    })

    return {
      routes,
      loading,
      isScrolled,
      footerAtTop,
      footerVisible,
      anchorScrolledOptions,
      footerVisibilityOptions,
      footerAtTopOptions,
      hasError,
      hasHero,
      hasDarkHero,
    }
  },
  computed: {
    ...mapGetters("layout", ["data"]),
  },
})
</script>
<style>
#app-main > div,
#app-main > section {
  min-height: calc(100vh - 346px);
}

#app-main > div.page-loader {
  min-height: calc(100vh - 261px);
}
</style>
