export type IProfession = {
  title: string
  image: string
}
