// eslint-disable-next-line import/named
import { RawLocation } from "vue-router"
import { NuxtError } from "@nuxt/types/app"

export interface IPageRoute {
  name: string
  to: RawLocation
  icon?: string
  active?: boolean
  exact?: boolean
  children?: IPageRoute[]
  hidden?: boolean
}

export interface IPageError extends NuxtError {
  description?: string
  linkText?: string
  linkName?: string
  seoTitle?: string
}

export type PageRoutes = IPageRoute[]

export interface IBreadCrumb {
  text: string
  to?: RawLocation
}

type Dictionary<T> = { [key: string]: T }

export type IQuery = Dictionary<string | (string | null)[]>
