export interface Sys {
  id: string
}

export interface Entry {
  sys: Sys
}
