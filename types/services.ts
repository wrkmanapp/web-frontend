/* eslint-disable camelcase */
import { User } from "~/types/content"

export interface loginData {
  token: string
  user: User
}

export interface ArtisansAroundPayload {
  location: number[]
  service: string
}
