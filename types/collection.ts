export interface ICollection<T> {
  total: number
  limit: number
  skip: number
  items: Array<T>
}
