export interface IAsset {
  name: string
  url: string
  previewUrl?: string
  size?: number
  height?: number
  width?: number
}
