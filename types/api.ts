export interface ApiData<T> {
  data: T
  message: string
  errors: any
  success: boolean
}
