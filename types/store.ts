import {
  IAboutContent,
  IContactContent,
  IHomeContent,
  ILayoutContent,
  IServiceArtisans,
  IServicesContent,
  TServiceContent,
} from "~/types/content"

export type LayoutState = {
  data?: ILayoutContent
  contact?: IContactContent
}

export type HomeState = {
  data?: IHomeContent
}

export type AboutState = {
  data?: IAboutContent
}

export type IServicesState = {
  data?: IServicesContent[]
  serviceArtisans?: IServiceArtisans[]
  service?: TServiceContent
}

export type IArtisanState = {
  data?: IServiceArtisans
}

export type AuthState = {
  loading: Boolean
  user: any
  token: string
}
