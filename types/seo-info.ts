import { IMaps } from "~/types/map"

export interface ISeoInfo {
  pageTitle: string
  pageDescription: string
  metaTags: IMaps
}
