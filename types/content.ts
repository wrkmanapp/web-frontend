import { ISeoInfo } from "~/types/seo-info"
import { IAsset } from "~/types/asset"
import { IProfession } from "~/types/professions"

export type IFeature = {
  featureTitle: string
  featureDescription: string
  featureImage: IAsset
  id: string
}

export type IService = {
  serviceTitle: string
  serviceImage: IAsset
  id: string
}

export type IHomeAbouts = {
  caption: string
  aboutImage: IAsset
  id: string
}

export type IFaqs = {
  question: string
  answer: string
  id: string
}

export type ITeam = {
  id: string
  avatar: IAsset
  name: string
  role: string
}
export type ILinkItems = {
  name: string
  to: string
}
export type IMediaLinkItems = {
  socialLink: string
  socialName: string
}

export type ITestimonials = {
  id: string
  title: string
  image: IAsset
  text: string
}

export interface ILayoutContent {
  seoInfo: ISeoInfo
  navLogo: IAsset
  copyrightText: string
  termsLinkText: string
  loginLinkText: string
  footerGetHelpTitle: string
  footerCompanyTitle: string
  provideHelpLinkText: string
  footerLogo: IAsset
  getHelpLinks: ILinkItems[]
  companyLinks: ILinkItems[]
  authLinks: ILinkItems[]
  mediaLinks: IMediaLinkItems[]
}

export interface IHomeContent {
  seoInfo: ISeoInfo
  introDescription: string
  professions: Array<IProfession>
  features: Array<IFeature>
  servicesIntroTitle: string
  servicesButtonText: string
  services: Array<IService>
  aboutTitle: string
  aboutDescription: string
  homeAbouts: Array<IHomeAbouts>
  appStoreImage: IAsset
  handImage: IAsset
  appStoreIntroTitle: string
  appStoreIntroDescription: string
  appStoreLink: string
  playStoreLink: string
  appStoreIcon: IAsset
  playStoreIcon: IAsset
  faqs: Array<IFaqs>
  homeTestimonials: ITestimonials[]
  testimonialTitle: string
}

export interface IAboutContent {
  seoInfo: ISeoInfo
  introTitle: string
  introImage: IAsset
  heroDescription: string
  teams: Array<ITeam>
  heroImage: IAsset
  whatWeOfferText: string
  whatWeOfferContent: string
}

export interface IContactContent {
  introTitle: string
  phone: string
  address: string
  email: string
  buttonText: string
}

export interface IServicesContent {
  _id: string
  status: boolean
  name: string
  artisans: number
  slug: string
}

export interface IServiceArtisans {
  skills: string[]
  photos: string[] | string
  verified: boolean
  locationCoordinates: number[]
  _id: string
  id: string
  email: string
  address: string
  area: string
  bio: string
  country: string
  firstName: string
  lastName: string
  locationName: string
  phone: string
  picture: string
  ratings: number
  serviceType: string
  state: string
  totalJobs: number
  type: string
  deviceToken: string
  authCode: string
}
export interface TServiceContent {
  seoInfo: ISeoInfo
  introTitle: string
  serviceImage: IAsset
  servicesAroundImage: IAsset
}

export interface User {
  verified: Boolean
  suspended: Boolean
  _id: string
  email: string
  area: string
  country: string
  firstName: string
  lastName: string
  phone: string
  authCode: string
  deviceToken: string
  location: number[] | undefined
}
