import { Middleware } from "@nuxt/types"

/**
 * array of pages that are authenticated
 */
const includes: Array<String> = ["/dashboard"]

const authMiddleWare: Middleware = ({ redirect, store }) => {
  const token = store.getters["auth/getToken"]
  const routeName = store.getRouteBaseName()
    ? store.getRouteBaseName()!.toLowerCase()
    : ""
  const isAuthPage = includes.includes(routeName)

  // if token is not defined and user is not on authenticated pages, redirect to login
  if (!token && isAuthPage) {
    redirect("/login")
  }
}

export default authMiddleWare
