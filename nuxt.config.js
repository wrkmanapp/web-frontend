import defaultTheme from "tailwindcss/defaultTheme"
const isProd = process.env.NODE_ENV === "production"

const publicRuntimeConfig = {
  appUrl: process.env.APP_URL,
}

const privateRuntimeConfig = {
  strHost: process.env.STR_HOST,
  baseUrl: process.env.BASE_URL || "",
}

export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: "WrkMan",
    titleTemplate: "%s | Wrkman Solutions Limited",
    htmlAttrs: {
      lang: "en",
    },
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      { hid: "description", name: "description", content: "" },
      { name: "format-detection", content: "telephone=no" },
      {
        hid: "keywords",
        name: "keywords",
        content: "",
      },
      {
        hid: "msapplication",
        name: "msapplication-TileColor",
        content: "#0077FF",
      },
      {
        hid: "theme-color",
        name: "theme-color",
        content: "#0077FF",
      },
    ],
    link: [{ rel: "icon", type: "image/x-icons", href: "/favicon.ico" }],
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    "~/assets/css/rasa.css",
    "~/assets/css/lato.css",
    "~/assets/css/main.css",
    "~/assets/css/animations.scss",
    "~/assets/css/main.scss",
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    { src: "~/plugins/intersection-observer", ssr: false },
    { src: "~/plugins/click-outside", ssr: false },
    { src: "~/plugins/vuelidate" },
    "~/plugins/element-ui",
    "~/plugins/persistedState",
    "~/plugins/persistedState.client",
    "~/plugins/element-ui",
    "~/plugins/axios",
    "~/plugins/auth",
    "~/plugins/errorHandler",
    "~/plugins/layout",
  ],
  router: {
    middleware: "auth",
    prefetchLinks: false,
  },
  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://composition-api.nuxtjs.org/
    "@nuxtjs/composition-api/module",
    // https://go.nuxtjs.dev/typescript
    "@nuxt/typescript-build",
    // https://go.nuxtjs.dev/stylelint
    "@nuxtjs/stylelint-module",
    // https://go.nuxtjs.dev/tailwindcss
    "@nuxtjs/tailwindcss",
    // https://github.com/nuxt-community/device-module
    "@nuxtjs/device",
    // https://github.com/nuxt-community/svg-module
    "@nuxtjs/svg",
    // https://github.com/postcss/postcss/wiki/PostCSS-8-for-end-users
    "@nuxt/postcss8",
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/axios
    "@nuxtjs/axios",
    // https://go.nuxtjs.dev/pwa
    "@nuxtjs/pwa",
    // https://i18n.nuxtjs.org/setup/
    "@nuxtjs/i18n",
    // https://github.com/nuxt-community/apollo-module
    ["@nuxtjs/apollo", { mode: "server" }],
    // https://github.com/nuxt-community/markdownit-module
    "@nuxtjs/markdownit",
    // https://www.npmjs.com/package/cookie-universal-nuxt
    "cookie-universal-nuxt",
  ],

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {},
  tailwindcss: {
    jit: true,
    exposeConfig: true,
    config: {
      darkMode: "class", // or 'media' or 'class'
      purge: {
        content: ["~/node_modules/tailwindcss-dark-mode/prefers-dark.{js,ts}"],
      },
      theme: {
        darkSelector: ".dark-mode",
        extend: {
          colors: {
            primary: {
              500: "#0077FF",
              400: "#28BEE0",
              300: "#2046CB",
              200: "#568B91",
              100: "#4477C4",
            },
            grey: {
              100: "#444854",
              200: "#575A65",
              300: "#9DA8B6",
              400: "#C0CCDA",
              500: "#D3DCE6",
              600: "#E0E6ED",
              700: "#E5E9F2",
              800: "#EFF2F7",
              900: "#F9FAFC",
              1000: "#979AA5",
            },
            gray1: "#444854",
            gray2: "#575A65",
            gray3: "#9DA8B6",
            gray4: "#C0CCDA",
            gray5: "#D3DCE6",
            gray6: "#E1EBF5",
            gray7: "#E5E9F2",
            gray8: "#EFF2F7",
            gray9: "#f9fafc",
            "caribbean-green": "#00C3B3",
            "dark-yellow": "#EBBE00",
            "light-pink": "#FFE1E0",
            "red-105": "#FFE1E0",
            body: "#1D1E2C",
          },
          transformOrigin: {
            0: "0%",
          },
          lineHeight: {
            12: "3rem",
            13: "3.5rem",
            14: "4rem",
            15: "4.5rem",
          },
          zIndex: {
            "-1": "-1",
            10: 10,
            20: 20,
            25: 25,
            30: 30,
            40: 40,
            50: 50,
            75: 75,
            100: 100,
            max: 9999,
          },
          borderRadius: {
            "2lg": "2rem",
            "3lg": "2.5rem",
            "4xl": "4rem",
          },
        },
        fontFamily: {
          sans: ["lato", ...defaultTheme.fontFamily.sans],
          serif: ["rasa", ...defaultTheme.fontFamily.serif],
        },
        screens: {
          xs: "425px",
          ...defaultTheme.screens,
        },
      },
      variants: {
        extend: {
          fill: ["hover", "focus"],
        },
        backgroundColor: [
          "dark",
          "dark-hover",
          "dark-group-hover",
          "dark-even",
          "dark-odd",
        ],
        borderColor: [
          "dark",
          "dark-disabled",
          "dark-focus",
          "dark-focus-within",
        ],
        textColor: ["dark", "dark-hover", "dark-active", "dark-placeholder"],
      },
      plugins: [
        require("tailwindcss-dark-mode")(),
        require("@tailwindcss/line-clamp"),
      ],
    },
  },
  colorMode: {
    preference: "system",
    fallback: "light",
    hid: "nuxt-color-mode-script",
    globalName: "__NUXT_COLOR_MODE__",
    componentName: "ColorScheme",
    classPrefix: "",
    classSuffix: "",
    storageKey: "nuxt-color-mode",
  },
  device: {
    refreshOnResize: true,
  },

  i18n: {
    locales: [
      {
        code: "en",
        name: "English",
      },
      {
        code: "fr",
        name: "Français",
      },
    ],
    defaultLocale: "en",
    vueI18nLoader: true,
    detectBrowserLanguage: {
      alwaysRedirect: false,
      fallbackLocale: "en",
      onlyOnRoot: true,
      useCookie: true,
      cookieCrossOrigin: false,
      cookieDomain: null,
      cookieKey: "i18n_redirected",
      cookieSecure: isProd,
    },
    vueI18n: {
      fallbackLocale: "en",
    },
  },

  apollo: {
    clientConfigs: {
      default: "~/plugins/apollo.ts",
    },
  },

  markdownit: {
    preset: "default",
    runtime: true, // Support `$md()`
    linkify: true,
    breaks: true,
  },

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {
    baseUrl: process.env.BASE_URL,
  },

  publicRuntimeConfig,
  privateRuntimeConfig,

  serverMiddleware: ["~/server-middleware/api.ts"],
}
