module.exports = {
  extends: ["stylelint-config-standard", "stylelint-config-prettier"],
  // add your custom config here
  // https://stylelint.io/user-guide/configuration
  rules: {
    "at-rule-no-unknown": [
      true,
      {
        ignoreAtRules: [
          "tailwind",
          "layer",
          "apply",
          "variants",
          "responsive",
          "screen",
          "use",
          "forward",
          "mixin",
          "include",
        ],
      },
    ],
    "font-family-no-missing-generic-family-keyword": [
      true,
      {
        ignoreFontFamilies: ["rasa", "lato"],
      },
    ],
  },
}
