import bodyParser from "body-parser"
import express from "express"
import { AxiosResponse } from "axios"
const axios = require("axios")
const app = express()

const env = {
  baseUrl: process.env.BASE_URL,
}
// @ts-ignore
app.use(bodyParser.json())

app.post("/api/fetch-artisans-around", async (req, res) => {
  try {
    axios
      .post(`${env.baseUrl}/app/artisansAround`, req.body)
      .then((result: AxiosResponse<any>) => {
        res.json({
          data: result.data.data,
          success: true,
          message: "artisansAround",
        })
      })
      .catch((err: any) => {
        res.json({
          data: null,
          errors: {
            message: err.message,
          },
        })
      })
  } catch (e) {
    res.json({
      data: null,
      errors: {
        message: "Internal server error, please contact admin",
      },
      success: false,
    })
  }
})

module.exports = app
