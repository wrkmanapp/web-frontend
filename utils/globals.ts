export const getCurrentLocation = (): Promise<GeolocationPosition> => {
  // get position
  return new Promise((resolve) => {
    navigator.geolocation.getCurrentPosition(
      (pos) => {
        resolve(pos)
      },
      (err) => {
        throw new Error(err.message)
      }
    )
  })
}
